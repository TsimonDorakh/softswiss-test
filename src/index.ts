import {StoneScene} from "./StoneScene";

const getSpeed = () => document.getElementById('speed').value;
const getAngle = () => document.getElementById('angle').value;
const getDuration = () => document.getElementById('duration').value;
const prepareOptions = () => ({
    speed: getSpeed(),
    angle: getAngle(),
    duration: getDuration(),
});

window.onload = () => {
    let stoneScene = new StoneScene(prepareOptions());

    document.getElementById('restartButton').addEventListener('click', () => {
        stoneScene.restart(prepareOptions());
    });
};


