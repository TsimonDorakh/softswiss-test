import * as stoneImage from './assets/images/stone_t.png';
import * as spritePhysics from './assets/images/stone_t.json';

const WORLD_WIDTH: number = 800;
const WORLD_HEIGHT: number = 250;
const M_PIX: number = 20; // 1 meter == M_PIX pixels

const toRadians = angle => angle * (Math.PI / 180);

interface IStoneSceneOptions {
    angle: number;
    speed: number;
    duration: number;
}

export class StoneScene {
    private game: Phaser.Game;
    private bitmapData: Phaser.BitmapData;
    private stone: Phaser.Sprite;
    private stopped: boolean = false;
    private timeoutId: number;

    constructor(private options: IStoneSceneOptions) {
        this.game = new Phaser.Game(WORLD_WIDTH, WORLD_HEIGHT, Phaser.CANVAS, 'content', {
            preload: this.preload.bind(this),
            create: this.create.bind(this),
            update: this.update.bind(this),
        });
    }

    public restart(options: IStoneSceneOptions): void {
        clearTimeout(this.timeoutId);

        this.options = options;
        this.stopped = false;

        this.game.state.restart();
    }

    private stopAnimation(): void {
        this.stopped = true;
    }

    private update(): void {
        if (this.stopped) {
            this.stone.body.static = true;
            this.stone.body.velocity.x = 0;
            this.stone.body.velocity.y = 0;
            this.stone.body.angularVelocity = 0;
        }

        this.bitmapData.context.fillStyle = '#ffff00';
        this.bitmapData.context.fillRect(
            this.stone.x - this.stone.width / 2,
            this.stone.y + this.stone.height / 2,
            2, 2);
    }

    private preload() {
        this.game.load.image('stone', stoneImage);

        this.timeoutId = setTimeout(this.stopAnimation.bind(this), this.options.duration * 1000);
    }

    private create() {
        this.game.physics.startSystem(Phaser.Physics.P2JS);
        this.game.physics.p2.gravity.y = 9.81 * M_PIX;

        this.game.stage.backgroundColor = '#124184';

        this.bitmapData = this.game.add.bitmapData(WORLD_WIDTH, WORLD_HEIGHT);
        this.bitmapData.context.fillStyle = '#ffffff';
        this.game.add.sprite(0, 0, this.bitmapData);

        this.stone = this.game.add.sprite(0, WORLD_HEIGHT - 1, 'stone');

        this.game.physics.p2.enable(this.stone, false);
        this.stone.anchor.setTo(0.5, 0.5);
        this.stone.reset(this.stone.width / 2, WORLD_HEIGHT - this.stone.height / 2);

        this.stone.body.mass = 10000;
        this.stone.body.clearShapes();
        this.stone.body.loadPolygon(null, spritePhysics.stone_t);
        this.stone.body.angularVelocity = Math.random() * 2;
        this.stone.body.velocity.x = M_PIX * this.options.speed * Math.cos(toRadians(this.options.angle));
        this.stone.body.velocity.y = -M_PIX * this.options.speed * Math.sin(toRadians(this.options.angle));
    }
}